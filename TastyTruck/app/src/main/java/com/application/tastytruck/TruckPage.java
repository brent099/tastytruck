package com.application.tastytruck;


import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class TruckPage extends MainActivity {


    private ListView menuListView;
    private ListView hoursListView;
    private ListView reviewsListView;

    private ArrayList<String> list = new ArrayList<String>(); //for sample data, delete me

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    private ArrayAdapter<String> adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_page);
        menuListView = findViewById(R.id.truckMenuListView);
        hoursListView = findViewById(R.id.truckHoursListView);
        reviewsListView = findViewById(R.id.truckReviewListView);

        createRandomData();

        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, list);
        menuListView.setAdapter(adapter);
        hoursListView.setAdapter(adapter);
        reviewsListView.setAdapter(adapter);


    }

    /**
     * just to fill data, delete when real data is in.
     */
    public void createRandomData(){
        System.out.println("in get random");
        for(int i = 0; i < 15; i++){
            list.add("Test # " + i);
        }
    }
}
