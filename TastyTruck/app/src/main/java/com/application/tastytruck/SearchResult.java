package com.application.tastytruck;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class SearchResult extends MainActivity implements AdapterView.OnItemSelectedListener  {

    //Button gotoHome;
    ListView listView;
    TextView listItemTextView;
    ListAdapter arrayAdapter;


    ArrayList<String> list = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);


        //Spinner stuff
        Spinner spinner = (Spinner) findViewById(R.id.categoriesSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        System.out.println("in on create");
        createRandomData();


        arrayAdapter = new ListAdapter(this, list);
        arrayAdapter.notifyDataSetChanged(); // Update data shown to user

        //Creating list elements based on list
        listView = findViewById(R.id.searchListView);

        arrayAdapter.notifyDataSetChanged();
        listView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                //Where it directs to truck page
                System.out.println("Directing once implemented yay!");

                Intent intent = new Intent(SearchResult.this, TruckPage.class);
                startActivity(intent);



            }
        });

    }

    /**
     * just to fill data, delete when real data is in.
     */
    public void createRandomData(){
        System.out.println("in get random");
        for(int i = 0; i < 15; i++){
            list.add("Test # " + i);
        }
    }


    public class ListAdapter extends ArrayAdapter<String> {
        public ListAdapter(Context context, ArrayList<String> list) {
            super(context, 0, list);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            System.out.println("in getview");
            String s = getItem(position);
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            }
            listItemTextView = (TextView) convertView.findViewById(R.id.readingId);
            listItemTextView.setText("SAMPLE " + s);


            // Return and show on screen
            return convertView;

        }


    }

    /**
     * This is the method that grabs the items from the spinner.
     *
     * @param parent
     * @param view
     * @param pos
     * @param l
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
        //Gets the text from the spinner dropdown.
        String spinnerText = parent.getItemAtPosition(pos).toString();
        //This toast will give the user some feedback of their selected category.
        Toast.makeText(parent.getContext(), spinnerText, Toast.LENGTH_SHORT).show();
        //Test message for Logcat
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

}